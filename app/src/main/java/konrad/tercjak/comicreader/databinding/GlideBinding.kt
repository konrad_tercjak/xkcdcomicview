//package konrad.tercjak.comicreader.databinding
//
//import android.widget.ImageView
//import com.bumptech.glide.Glide
//import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
//import androidx.databinding.BindingAdapter //Jetpack only
//
//@BindingAdapter("imageFromUrl")
//fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
//    if (!imageUrl.isNullOrEmpty()) {
//        Glide.with(view.context)
//            .load(imageUrl)
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .into(view)
//    }
//
//}