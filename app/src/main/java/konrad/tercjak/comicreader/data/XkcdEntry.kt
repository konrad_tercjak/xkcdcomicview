package konrad.tercjak.comicreader.data

import com.squareup.moshi.Json

data class XkcdEntry(
    @field:Json(name = "title") public val title:String,
    @field:Json(name = "alt") public val desc:String,
    @field:Json(name = "img")public val url:String)
