package konrad.tercjak.comicreader.service

import konrad.tercjak.comicreader.data.XkcdEntry
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface XkcdApi {
    @GET("{id}/info.0.json")
    fun getEntry(
        @Path("id") id: Int
    ): Call<XkcdEntry>

}