package konrad.tercjak.comicreader.viewmodel

import android.app.Application
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder
import com.bumptech.glide.request.RequestOptions
//import com.squareup.picasso.Picasso
import konrad.tercjak.comicreader.data.XkcdEntry
import konrad.tercjak.comicreader.service.XkcdApi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import android.graphics.Bitmap
import android.view.View
import android.widget.Button
import android.widget.ToggleButton
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.bumptech.glide.request.target.SimpleTarget




class XkcdViewModel(
    val imageView: ImageView,
    val titleTextView: TextView

//   , val descTextView: TextView
) {

  private val requestBuilder: RequestBuilder<Drawable>
    var id = 0;
    private val xkcdApi: XkcdApi // by Delegates.notNull()

    val handler = Handler()
    val executor = Executors.newFixedThreadPool(1)

    init {
        val requestOptions=RequestOptions().format(DecodeFormat.PREFER_ARGB_8888)
        requestBuilder= Glide.with(imageView.context).asDrawable().apply(requestOptions);

        val retrofit = Retrofit.Builder()
            .baseUrl("https://xkcd.com/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        xkcdApi = retrofit.create(XkcdApi::class.java)

//        imageView.setOnTouchListener(object : OnSwipeTouchListener(imageView.context) {
//            override fun onSwipeLeft() {
//                prvEntry()
//            }
//
//            override fun onSwipeRight() {
//                nextEntry()
//            }
//        })

    }


    public  fun nextEntry() {
        newEntry(id +1)

    }

public    fun prvEntry() {
        if (id > 0) {
            newEntry(id - 1)
        }
    }

    fun newEntry(newID: Int) {
        id = newID
        val entry = xkcdApi.getEntry(id)

        val worker: Callable<XkcdEntry?> = Callable<XkcdEntry?> {
            val response = entry.execute().body()
            return@Callable response
        }

        executor.submit(worker).get()?.let {
            setImageView(it.url)
//            setTextView(descTextView, it.desc)
            setTextView(titleTextView, it.title+" #"+id)
//            Log.d("sss", it.desc)
        }//.execute(worker)

//        response?.let {
//            setImageView(it.url)
//            setTextView(descTextView, it.desc)
//            setTextView(titleTextView, it.title)
//        }


    }

    fun setImageView(imageUrl: String?) {
        if (!imageUrl.isNullOrEmpty()) {
            imageView.layout(0, 0, 0, 0);

//            Picasso.get
            requestBuilder
//            Glide.with(imageView.context)
//                .setDefaultRequestOptions(requestOptions)
               .load(imageUrl).into(imageView)
//            imageView.
        }
    }


    fun setTextView(textView: TextView, text: String) {
        if (!text.isNullOrEmpty()) {
            textView.text = text
        }
    }
}
